package com.everis.MesssageApi;

import com.everis.MesssageApi.models.Message;
import com.everis.MesssageApi.repository.MessageRepository;
import com.everis.MesssageApi.service.MessageService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.server.ResponseStatusException;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class MesssageApiApplicationTests {

	@InjectMocks
	private MessageService service;

	@Mock
	private MessageRepository repository;

	@BeforeMethod
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	//Teste GetAll com lista populada
	@Test()
	public void messageTestGetAll(){
		final List<Message> list = new ArrayList<>();
		Message message = new Message();

		message.setId("5d77bc7269976d444c774862");
		message.setMessage("payload,{ }");

		list.add(message);

		Mockito.when(repository.findAll()).thenReturn(list);
		service.findAll();
	}

	//Teste Get All com lista vazia
	@org.testng.annotations.Test(expectedExceptions = ResponseStatusException.class)
	public void messageNullTestGetAll() {
		final List<Message> list = new ArrayList<>();
		Mockito.when(repository.findAll()).thenReturn(list);
		service.findAll();
	}

	//Teste GetById
	@Test()
	public void messageTestGetById(){
		Message message = new Message();
		String id = "5d77bc7269976d444c774862";
		message.setId("5d77bc7269976d444c774862");

		Optional<Message> optionalMessage = Optional.of(message);

		Mockito.when(repository.findById(id)).thenReturn(optionalMessage);
		service.findById(id);
	}

	//Teste Insert com objeto populado
	@Test()
	public void messageTestInsert(){
		Message message = new Message();

		message.setId("5d77bc7269976d444c774862");
		message.setMessage("Test");

		Mockito.when(repository.insert(message)).thenReturn(message);

		service.insert(message);
	}

	//Teste Update com id válido
	@Test()
	public void messageTestUpdate() {
		Message message = new Message();
		message.setId("5d77bc7269976d444c774862");

		Optional<Message> optAgency = Optional.of(message);

		Mockito.when(repository.save(message)).thenReturn(message);
		Mockito.when(repository.findById(message.getId())).thenReturn(optAgency);

		Message response = service.update(message);
		Assert.assertEquals(response.getId(), message.getId());
	}

	//Teste Update com id inválido
	@org.testng.annotations.Test(expectedExceptions = ResponseStatusException.class)
	public void messageNullTestUpdate() {

		Message message = new Message();
		message.setId("5d77bc7269976d444c774862");

		Optional<Message> optAgency = Optional.empty();

		Mockito.when(repository.findById(message.getId())).thenReturn(optAgency);

		service.update(message);
	}

	//Teste Delete
	@Test()
	public void messageTestDelete() {

		String id ="5d77bc7269976d444c774862";

		Message agency = new Message();
		agency.setId("5d77bc7269976d444c774862");

		Optional<Message> optAgency = Optional.of(agency);
		Mockito.when(repository.findById(agency.getId())).thenReturn(optAgency);

		Message response = service.delete(id);
		Assert.assertEquals(response.getId(), agency.getId());
	}

}
