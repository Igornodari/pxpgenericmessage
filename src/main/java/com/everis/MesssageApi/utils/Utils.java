package com.everis.MesssageApi.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.UUID;

public class Utils {
	private static Gson gson = new Gson();

	public static <T extends Object> T toObject(String json, Class<T> type) {
		return gson.fromJson(json, type);
	}
	
	public static String toJson(Object obj) {
		if (obj == null) {
			return null;
		}
		gson = new GsonBuilder().disableHtmlEscaping().create();
		return gson.toJson(obj);
	}

	public static String toString(boolean bool) {
		if (bool) {
			return "true";
		} else {
			return "false";
		}
	}

	public static String GenerateUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
}
