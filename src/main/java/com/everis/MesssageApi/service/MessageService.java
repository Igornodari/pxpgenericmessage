package com.everis.MesssageApi.service;


import com.everis.MesssageApi.models.Message;
import com.everis.MesssageApi.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
public class MessageService {

    @Autowired
    MessageRepository messageRepository ;

    public void insert(Message message) {
        messageRepository.insert(message);
    }

    //PUT
    public Message update(@RequestBody @Valid Message message) {
        Optional<Message> messageFind = messageRepository.findById(message.getId());
        if (!messageFind.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ERROR: ID NOT FOUND");
        } else {
            return messageRepository.save(message);
        }
    }
    public List<Message> findAll() {
        List<Message> messagelist = messageRepository.findAll();
        if (messagelist.isEmpty() || messagelist == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " message list is empty or null");
        } else {
            return messagelist;
        }
    }

    public Optional<Message> findById(String id) {
        return messageRepository.findById(id);
    }

    public Message delete(String id) {
        Optional<Message> device = this.messageRepository.findById(id);
        if (device.isPresent()) {
            this.messageRepository.deleteById(id);
        }
        return device.orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Id not found"));
    }
    }

