package com.everis.MesssageApi.repository;

import com.everis.MesssageApi.models.Message;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MessageRepository extends MongoRepository<Message, String> {}

