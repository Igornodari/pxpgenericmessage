package com.everis.MesssageApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MesssageApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MesssageApiApplication.class, args);
	}

}
