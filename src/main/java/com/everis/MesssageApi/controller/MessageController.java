package com.everis.MesssageApi.controller;



import com.everis.MesssageApi.models.Message;
import com.everis.MesssageApi.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("message/api")
public class MessageController {

    @Autowired
    private MessageService messageService;


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Message>> findAll() {
        List<Message> list = this.messageService.findAll();
        return ResponseEntity.ok(list);
    }

    // GET BY ID
    @GetMapping("/{id}")
    public ResponseEntity<Message> getById(@PathVariable String id) {
        Optional<Message> messages = this.messageService.findById(id);
        return ResponseEntity.ok().body(
                messages.orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "getById Bad Request")));
    }

    @PostMapping
    public ResponseEntity<Void> insert(@RequestBody @Valid Message message) {
        this.messageService.insert(message);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(message.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @PutMapping
    public void update(@RequestBody @Valid Message message) {
        this.messageService.update(message);
    }

    // DELETE
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        this.messageService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
